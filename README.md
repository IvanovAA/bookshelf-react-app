This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# task requirements

Develop a Bookshelf app that lets you browse lists books, add, edit, and delete books.

The functionality you need:

## 1. Displaying a list of books:
Each book appears in a table line, at the end of each line
there are editing or book delete buttons.
 ### Comment
Each book has the following fields:
 - Image of the book;
 - The author of the book (string value, no restrictions);
 - The title of the book (string value, no restrictions);
 - Rating (number value from 1 to 10 - step 0.1);
 - Year of release (number, no more than 2017).

## 2. Editing a book:
When you click on the "Edit" button in the book line, the form opens
editing, which has already set up data about the edited
Books. The form should have save and undo buttons. When you click on
first, the book data is stored, the data in the table is updated. When
click on the second data to return to its original state, even if
edhisks have been made.
 ### Comment
A book image is a link to an image on the Internet or a file name if
Images lie locally. There is no need to download images.

## 3. Adding a new book:
When you click on the "Add book" button, an empty form opens,
that you can provide book data. The shape should have buttons
"Save" and "Cancel." When you click on the first list of books is added
a new book with inputed data. If you click on the "Cancel" form
closes, a new book is added, the list of books remains without
Changes.
 ### Comment
A book image is a link to an image on the Internet or a file name if they
lie locally. There is no need to implement image downloads.

## 4. Removing the book:
When you click on the Delete button, the book is deleted and the list of books is updated.

# Available Scripts

In the project directory, you can run:

### `npm install -g json-server` - install JSON Server
### `npm i`

### `json-server --watch db.json` in 1-st console

Runs local server (http://localhost:3000)

### `npm start` in 2-nd console

Runs the app in the development mode (http://localhost:3001)
