import React from "react";
import Main from "./Components/Main/Main";
import Header from "./Components/Header/Header";
import Footer from "./Components/Footer/Footer";

export default class App extends React.Component {
  state = {
    books: [],
    isFormVisible: false,
    isEditMode: false,
    bookProperties: {
      title: "",
      author: "",
      year: "",
      rating: "",
      url: ""
    },
    isFormComplete: false
  };

  bookInitialId = 4;

  toggleFormHandler = () => {
    this.setState({
      isFormVisible: !this.state.isFormVisible
    });
  };

  clearForm = () => {
    this.setState({
      bookProperties: {
        title: "",
        author: "",
        year: "",
        rating: "",
        url: ""
      }
    });
  };

  cancelForm = () => {
    this.toggleFormHandler();
    this.clearForm();
    this.setState({
      isEditMode: false,
      isFormComplete: false
    });
  };

  inputHandleChange = event => {
    const target = event.target;
    const name = target.name;

    this.setState(
      {
        bookProperties: {
          ...this.state.bookProperties,
          [name]: target.value
        }
      },
      this.isFormFilled
    );
  };

  addBookHandler = () => {
    this.setState({
      books: [
        ...this.state.books,
        {
          ...this.state.bookProperties,
          id: this.bookInitialId++
        }
      ]
    });
    this.cancelForm();
  };

  toggleEditModeHandler = () => {
    this.setState({
      isEditMode: !this.state.isEditMode
    });
  };

  editBookHandler = id => {
    this.setState(
      {
        bookProperties: { ...this.state.books.find(item => id === item.id) }
      },
      this.isFormFilled
    );
    this.toggleEditModeHandler();
    this.toggleFormHandler();
  };

  addEditedBook = () => {
    const newBooksList = this.state.books.filter(
      item => item.id !== this.state.bookProperties.id
    );
    this.setState({
      books: [...newBooksList, { ...this.state.bookProperties }]
    });
    this.cancelForm();
    this.toggleEditModeHandler();
  };

  deleteHandler = id => {
    this.setState({
      books: this.state.books.filter(book => book.id !== id)
    });
  };

  sortBooksById = (prevBook, nextBook) => {
    if (prevBook.id > nextBook.id) {
      return 1;
    }
    return -1;
  };

  isFormFilled = () => {
    let controls = this.state.bookProperties;
    this.setState({
      isFormComplete:
        !!controls.title &&
        !!controls.author &&
        !!controls.year &&
        !!controls.rating &&
        !!controls.url
    });
  };

  componentDidMount() {
    fetch("http://localhost:3000/books")
      .then(response => response.json())
      .then(data =>
        this.setState({
          books: data
        })
      );
  }

  render() {
    return (
      <>
        <Header handleClick={this.toggleFormHandler} />
        <Main
          books={this.state.books}
          formState={this.state.isFormVisible}
          bookProperties={this.state.bookProperties}
          deleteHandler={this.deleteHandler}
          toggleFormHandler={this.toggleFormHandler}
          addBookHandler={this.addBookHandler}
          inputHandleChange={this.inputHandleChange}
          editBookHandler={this.editBookHandler}
          addEditedBook={this.addEditedBook}
          isEditMode={this.state.isEditMode}
          cancelForm={this.cancelForm}
          sortBooksById={this.sortBooksById}
          isFormComplete={this.state.isFormComplete}
        />
        <Footer />
      </>
    );
  }
}
