import React from "react";
import styles from "./Form.module.css";
import Button from "../Button/Button";

const Form = props => {
  return (
    <div className={styles.BackDrop}>
      <section className={styles.Form}>
        <h2>Add your book</h2>
        {props.isFormComplete ? null : (
          <span className={styles.formRule}>
            all entry fields must be filled*
          </span>
        )}
        <form>
          <label htmlFor="title">Title</label>
          <input
            type="text"
            id="title"
            onChange={props.inputHandleChange}
            value={props.bookProperties.title}
            name="title"
          />
          <label htmlFor="author">Author</label>
          <input
            type="text"
            id="author"
            onChange={props.inputHandleChange}
            value={props.bookProperties.author}
            name="author"
          />
          <label htmlFor="rating">rating (from 1 to 10)</label>
          <input
            type="number"
            step="0.1"
            min="1"
            max="10"
            id="rating"
            onChange={props.inputHandleChange}
            value={props.bookProperties.rating}
            name="rating"
          />
          <label htmlFor="year">year</label>
          <input
            type="number"
            min="0"
            max="2019"
            id="year"
            onChange={props.inputHandleChange}
            value={props.bookProperties.year}
            name="year"
          />
          <label htmlFor="url">cover</label>
          <input
            type="text"
            id="url"
            onChange={props.inputHandleChange}
            value={props.bookProperties.url}
            name="url"
          />
          <div className={styles.buttons}>
            {props.isEditMode ? (
              <Button
                handleClick={() => props.addEditedBook()}
                disabled={!props.isFormComplete}
              >
                add edited book
              </Button>
            ) : (
              <Button
                handleClick={() => props.addBookHandler()}
                disabled={!props.isFormComplete}
              >
                add book
              </Button>
            )}

            <Button handleClick={props.cancelForm}>cancel</Button>
          </div>
        </form>
      </section>
    </div>
  );
};
export default Form;
