import React from "react";
import styles from "./Main.module.css";

import Form from "../Form/Form";
import Book from "../Book/Book";

const Main = props => {
  return (
    <main className={styles.Main}>
      {props.books.sort(props.sortBooksById).map(book => {
        return (
          <Book
            key={book.id}
            title={book.title}
            author={book.author}
            rating={book.rating}
            year={book.year}
            url={book.url}
            id={book.id}
            deleteHandler={props.deleteHandler}
            editBookHandler={props.editBookHandler}
          />
        );
      })}
      {props.formState ? (
        <Form
          bookProperties={props.bookProperties}
          inputHandleChange={props.inputHandleChange}
          addBookHandler={props.addBookHandler}
          cancelForm={props.cancelForm}
          isEditMode={props.isEditMode}
          addEditedBook={props.addEditedBook}
          isFormComplete={props.isFormComplete}
        />
      ) : null}
    </main>
  );
};

export default Main;
