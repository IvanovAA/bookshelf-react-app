import React from "react";
import styles from "./Footer.module.css";

const Footer = () => (
  <footer className={styles.Footer}>Bookshelf React App</footer>
);

export default Footer;
