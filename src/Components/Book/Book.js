import React from "react";
import styles from "./Book.module.css";

import Button from "../Button/Button";

const Book = props => {
  const rateStyles = [styles.rating];

  if (props.rating < 4) {
    rateStyles.push(styles.low);
  } else if (props.rating < 7.5) {
    rateStyles.push(styles.middle);
  } else {
    rateStyles.push(styles.high);
  }

  return (
    <section className={styles.Book}>
      <div className={styles.info}>
        <img
          width="150"
          src={props.url}
          alt={props.title}
          className={styles.image}
        />
        <div className={styles.description}>
          <span className={styles.title}>{props.title}</span>
          <span className={styles.author}>{props.author}</span>
          <span className={rateStyles.join(" ")}>{props.rating}</span>
          <span className={styles.year}>{props.year}</span>
        </div>
      </div>
      <div className={styles.buttons}>
        <Button id={props.id} handleClick={props.editBookHandler}>
          edit
        </Button>
        <Button id={props.id} handleClick={props.deleteHandler}>
          delete
        </Button>
      </div>
    </section>
  );
};

export default Book;
