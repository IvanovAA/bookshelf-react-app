import React from "react";
import styles from "./Header.module.css";
import Button from "../Button/Button";

const Header = props => {
  return (
    <header className={styles.Header}>
      <h1 className={styles.appTitle}>bookshelf react app</h1>
      <Button handleClick={props.handleClick}>add book</Button>
    </header>
  );
};

export default Header;
