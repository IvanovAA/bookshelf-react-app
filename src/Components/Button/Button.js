import React from "react";
import styles from "./Button.module.css";

const Button = props => {
  const cls = [styles.Button];

  if (props.disabled) {
    cls.push(styles.disabled);
  }

  return (
    <button
      className={cls.join(" ")}
      type="button"
      onClick={() => props.handleClick(props.id)}
      disabled={props.disabled}
    >
      {props.children}
    </button>
  );
};

export default Button;
